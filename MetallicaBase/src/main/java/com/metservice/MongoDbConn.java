package com.metservice;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

public class MongoDbConn {

   public static void main( String args[] ){
      try{   
		MongoClient mongoClient = new MongoClient( "localhost" , 27017 );
		System.out.println("Database Connection Successful!");
        MongoDatabase db = mongoClient.getDatabase("metallicadb" );
        MongoCollection mc = db.getCollection("trades");
        Document doc = new Document("student", "Vikas")
                .append("name", "Arun")
                .append("class", 'V')
        		.append("rollno", 1);
        mc.insertOne(doc);
        System.out.println(mc);
        
	    }catch(Exception e){
	     System.err.println( e.getClass().getName() + ": " + e.getMessage() );
	  }
   }
}

